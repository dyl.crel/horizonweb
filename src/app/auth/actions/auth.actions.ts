import { createAction, props } from '@ngrx/store';

export const adminMode = createAction('[Auth] Admin Mode',props<{adminMode: boolean}>());
export const adminModeSuccess = createAction('[Auth] Admin Mode Success', props<{adminMode: boolean}>());
export const adminModeFailure = createAction('[Auth] Admin Mode Failure');

export const logout = createAction('[Auth] Logout');
export const logoutConfirmation = createAction('[Auth] Logout Confirmation');
export const logoutConfirmationDismiss = createAction(
  '[Auth] Logout Confirmation Dismiss'
);
