import { createAction, props } from '@ngrx/store';
import { Character } from '../models/character';


export const dashboard = createAction(
    '[Dashboard] Load Characters'
);

export const dashboardSuccess = createAction(
    '[Dashboard] Characters Success',
    props<{characters: Character[]}>()
    );
export const dashboardFailure = createAction(
    '[Dashboard] Characters Failure',
    props<{error:any}>()
);