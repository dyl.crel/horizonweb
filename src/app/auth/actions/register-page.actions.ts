import { Credentials, Account } from '../models';
import { props, createAction } from '@ngrx/store';


export const register = createAction('[Auth] Register', props<{user: Account}>());