import * as AuthActions from './auth.actions';
import * as AuthApiActions from './auth-api.actions';
import * as LoginPageActions from './login-page.actions';
import * as RegisterPageActions from './register-page.actions';
import * as DashboardPageActions from './dashboard-page.actions';

export { AuthActions, AuthApiActions, LoginPageActions, RegisterPageActions, DashboardPageActions};
