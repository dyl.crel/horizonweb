import { props, createAction } from '@ngrx/store';
import { Account, Credentials, RolesEnum } from '../models';

export const loginSuccess = createAction(
  '[Auth/API] Login Success',
  props<{ user: Account }>()
);
export const autoLogin = createAction(
  '[Auth/API] AutoLogin'
);

export const autoLoginSuccess = createAction(
  '[Auth/API] AutoLogin Success',
  props<{ user: Account }>()
);

export const autoLoginFailure = createAction(
  '[Auth/API] AutoLogin Failure',
  props<{ error: any }>()
);

export const loginFailure = createAction(
  '[Auth/API] Login Failure',
  props<{ error: any }>()
);

export const registerSuccess = createAction(
  '[Auth/API] Register Success',
  props<{ user: Account }>()
);

export const registerFailure = createAction(
  '[Auth/API] Register Failure',
  props<{ error: any }>()
);

export const role = createAction(
  '[Auth/API] Role',
  props<{ credentials: Credentials }>()
);

export const roleSuccess = createAction(
  '[Auth/API] Role Success',
  props<{ role: RolesEnum }>()
);

export const roleFailure = createAction(
  '[Auth/API] Role Failure',
  props<{ error: any }>()
);

export const autoRole = createAction(
  '[Auth/API] AutoRole'
);

export const autoRoleSuccess = createAction(
  '[Auth/API] AutoRole Success',
  props<{ role: RolesEnum }>()
);

export const autoRoleFailure = createAction(
  '[Auth/API] AutoRole Failure',
  props<{ error: any }>()
);

export const uploadProfileImage = createAction(
  '[Auth/API] Upload Image',
  props<{ profilImage: string }>()
);

export const uploadProfileImageSuccess = createAction(
  '[Auth/API] Upload Image Success',
  props<{ user: Account }>()
);

export const uploadProfileImageFailure = createAction(
  '[Auth/API] Upload Image Failure',
  props<{ error: any }>()
);

export const newsCount = createAction(
  '[Auth/API] News Count'
);
export const newsCountSuccess = createAction(
  '[Auth/API] News Count Success',
  props<{count: number}>()
);

export const loginRedirect = createAction('[Auth/API] Login Redirect');
export const HasloggedRedirect = createAction('[Auth/API] Logged Redirect');
