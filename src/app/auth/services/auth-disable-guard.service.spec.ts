import { TestBed } from '@angular/core/testing';

import { AuthDisableGuardService } from './auth-disable-guard.service';

describe('AuthDisableGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthDisableGuardService = TestBed.get(AuthDisableGuardService);
    expect(service).toBeTruthy();
  });
});
