import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';

import { Credentials, Account, RolesEnum } from '../models';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthApiActions } from '../actions';
import { AuthState } from '../reducers';
import { Store, select } from '@ngrx/store';
import * as fromAuth from '../reducers';
import * as fromRoot from '../reducers';
import { Character } from '../models/character';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private store: Store<fromRoot.State & fromAuth.State>) { }

  login({ username, password }: Credentials): Observable<any> {
    return this.http.post<Credentials>('https://localhost:44373/api/auth/login', { username, password });
  }

  logout() {
    localStorage.removeItem('nomyaUser');
  }

  register(user: Account): Observable<Account> {
    return this.http.post<Account>('https://localhost:44373/api/auth/registration', user);
  }
  getToken() {
    return localStorage.getItem('nomyaUser');
  }

  autoLogin(): Observable<Account> {
    return this.http.get<Account>('https://localhost:44373/api/auth/autologin');
  }
  autoRole(): Observable<RolesEnum> {
    return this.http.get<RolesEnum>('https://localhost:44373/api/auth/autorole');
  }

  getRole({ username, password }: Credentials): Observable<RolesEnum> {
    return this.http.post<RolesEnum>('https://localhost:44373/api/auth/role', { username, password });
  }
  getAdminMode(boolean: boolean): Observable<boolean>{
    return of(!boolean);
  }


  getCharacters() : Observable<Character[]>{
    return this.http.get<Character[]>('https://localhost:44373/api/character/personalcharacters/');
  }

  UploadImageProfil(linkImage: string) : Observable<Account>{
    return this.http.post<Account>('https://localhost:44373/api/auth/upload/profil/image', {linkImage});
  }

  getCountLikeNews() : Observable<number>{
    return this.http.get<number>('https://localhost:44373/api/auth/news/count');
  }

  isLogging() {
    let isLoggedIn = this.store.pipe(select(fromAuth.getLoggedIn));
    return isLoggedIn;
  }
}
