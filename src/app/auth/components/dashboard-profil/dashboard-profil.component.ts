import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Store } from '@ngrx/store';
import * as fromAuth from '../../reducers';
import { AuthApiActions } from 'app/auth/actions';
import { Account, RolesEnum } from 'app/auth/models';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard-profil.component.html',
  styleUrls: ['./dashboard-profil.component.scss']
})


export class DashboardProfilComponent implements OnInit {
  closeResult: string;
  urlImage: string;
  @Input() user: Account | null;
  @Input() urlImageError: string | null;
  @Input() error: string | null;
  @Input() likesnews: number | null;

  constructor(private modalService: NgbModal, private store : Store<fromAuth.State>) { }

  ngOnInit() {
  }
  open(content) {
    this.modalService.open(content, {centered : true});
  }
  sendImgProfil(){
    const profilImage = this.urlImage;
    this.store.dispatch(AuthApiActions.uploadProfileImage({profilImage}));
    this.urlImage = "";
      this.modalService.dismissAll();
  }
}