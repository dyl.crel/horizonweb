import { Component } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

/**
 * The dialog will close with true if user clicks the ok button,
 * otherwise it will close with undefined.
 */
@Component({
  template: `
  `,
  styles: [
    ` .dark-modal .modal-content {
      background-color: #292b2c;
      color: white;
  }
   .dark-modal .close {
      color: white;   
  }
    `,
  ],
})
export class LogoutConfirmationDialogComponent {}
