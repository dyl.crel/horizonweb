import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Credentials, Account } from '../models';
import { NgbDateStruct, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { stringify } from '@angular/compiler/src/util';

@Component({
  selector: 'bc-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss'],
})
export class RegisterFormComponent implements OnInit {
  // @Input()
  // set pending(isPending: boolean) {
  //   if (isPending) {
  //     this.form.disable();
  //   } else {
  //     this.form.enable();
  //   }
  // }

  @Input() errorMessage: string | null;
  passwordMatch: string | null;

  @Output() submitted = new EventEmitter<Account>();
  minDate: NgbDateStruct;
  form: FormGroup;


  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.minDate = { year: 1950, month: 1, day: 1 };
    this.form = this.formBuilder.group({
      email:['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(4)]],
      confirmpassword: ['', [Validators.required, Validators.minLength(4)]],
      firstname: ['', [Validators.required, Validators.minLength(4)]],
      lastname: ['', [Validators.required, Validators.minLength(4)]],
      username: ['', [Validators.required, Validators.minLength(6)]],
      sponsorid: [''],
      secretquestion: ['',[Validators.required, Validators.minLength(6)]],
      secretanswer: [null, [Validators.required, Validators.minLength(4)]],
      birthdate: ['', Validators.required]
    },{
      validators: MustMatch('password','confirmpassword')
    });
  }


  submit() {
    if (this.form.valid) {
      let dateComplete = this.form.controls['birthdate'].value;
      let birthday = new Date(Number.parseInt(dateComplete.year),Number.parseInt(dateComplete.month) - 1,Number.parseInt(dateComplete.day));
      this.form.controls['birthdate'].setValue(birthday.toLocaleString());
      let user: Account = this.form.value;
      this.submitted.emit(user);
    }
  }
}
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ mustMatch: true });
      } else {
          matchingControl.setErrors(null);
      }
  }
}