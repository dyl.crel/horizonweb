import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardVoteManagerComponent } from './dashboard-vote-manager.component';

describe('DashboardVoteManagerComponent', () => {
  let component: DashboardVoteManagerComponent;
  let fixture: ComponentFixture<DashboardVoteManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardVoteManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardVoteManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
