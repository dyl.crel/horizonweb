import { Component, OnInit, Input } from '@angular/core';
import { Character } from 'app/auth/models/character';

@Component({
  selector: 'app-dashboard-navigation',
  templateUrl: './dashboard-navigation.component.html',
  styleUrls: ['./dashboard-navigation.component.scss']
})
export class DashboardNavigationComponent implements OnInit {

  @Input() characters: Character[] | null;
  constructor() { }

  ngOnInit() {
  }

}
