import { createReducer, on } from '@ngrx/store';
import { DashboardPageActions } from '../actions';
import { Character } from '../models/character';


export interface State{
    error: string | null;
    characters: Character[] | null;
}

export const initialState: State = {
    error: null,
    characters: null
}

export const reducer = createReducer(
    initialState,

    on(DashboardPageActions.dashboard, state => ({
        ...state,
        error: null,
        characters: null
    })),

    on(DashboardPageActions.dashboardSuccess, (state, {characters}) => ({
        ...state,
        error: null,
        characters
    })),

    on(DashboardPageActions.dashboardFailure, (state, {error}) => ({
        ...state,
        error
    }))
)

export const getError = (state: State) => state.error;
export const getCharacters = (state: State) => state.characters;