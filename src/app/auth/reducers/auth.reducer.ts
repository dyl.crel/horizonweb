import { createReducer, on } from '@ngrx/store';
import { AuthApiActions, AuthActions } from '../actions';
import { Account, RolesEnum } from '../models';

export interface State {
  user: Account | null;
  role: RolesEnum | null;
  adminMode: Boolean;
  profilImage: string | null;
  error: string | null;
  count: number | null;
  pending: boolean;
}

export const initialState: State = {
  user: null,
  role: null,
  adminMode: false,
  profilImage: null,
  pending: false,
  error: null,
  count: null
};

export const reducer = createReducer(
  initialState,
  on(AuthApiActions.autoLogin, (state) => ({...state, pending: true})),
  on(AuthApiActions.loginSuccess, (state, { user }) => ({ ...state, user })),
  on(AuthApiActions.autoLoginSuccess, (state, { user }) => ({ ...state, user, pending: false})),
  on(AuthActions.logout, () => initialState),
  on(AuthApiActions.autoRole, (state) => ({...state})),
  on(AuthApiActions.roleSuccess, (state, { role }) => ({ ...state, role })),
  on(AuthApiActions.autoRoleSuccess, (state, { role }) => ({ ...state, role})),
  on(AuthActions.adminModeSuccess, (state, {adminMode}) => ({...state, adminMode})),
  on(AuthApiActions.uploadProfileImage, (state,{profilImage}) => ({...state, profilImage, error: null})),
  on(AuthApiActions.uploadProfileImageSuccess, (state, {user}) => ({...state, user, error:null})),
  on(AuthApiActions.uploadProfileImageFailure, (state, {error}) => ({...state, error})),
  on(AuthApiActions.newsCountSuccess, (state, {count}) => ({...state, count}))
);

export const getUser = (state: State) => state.user;
export const getRole = (state: State) => state.role;
export const getAdminMode = (state: State) => state.adminMode;
export const getProfilImage = (state: State) => state.profilImage;
export const getError = (state: State) => state.error;
export const getCountLikeNews = (state: State) => state.count;
export const getPendingStatus = (state: State) => state.pending;
