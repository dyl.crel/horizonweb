import { AuthApiActions, LoginPageActions } from '../actions';
import { createReducer, on } from '@ngrx/store';

export interface State {
  error: string | null;
  pending: boolean;
  token: string | null;
}

export const initialState: State = {
  error: null,
  pending: false,
  token: null
};

export const reducer = createReducer(
  initialState,
  on(LoginPageActions.login, state => ({
    ...state,
    error: null,
    pending: true,
  })),

  on(AuthApiActions.loginSuccess, state => ({
    ...state,
    error: null,
    pending: false,
    token: state.token
  })),
  on(AuthApiActions.loginFailure, (state, { error }) => ({
    ...state,
    error,
    pending: false,
  }))
);

export const getError = (state: State) => state.error;
export const getPending = (state: State) => state.pending;
export const getToken = (state : State) => state.token;
