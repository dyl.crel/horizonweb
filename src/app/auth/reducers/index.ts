import {
  createSelector,
  createFeatureSelector,
  Action,
  combineReducers,
} from '@ngrx/store';
import * as fromAuth from '../reducers/auth.reducer';
import * as fromLoginPage from '../reducers/login-page.reducer';
import * as fromRegisterPage from '../reducers/register-page.reducer';
import * as fromDashboardPage from '../reducers/dashboard-page.reducer';

export interface AuthState {
  status: fromAuth.State;
  loginPage: fromLoginPage.State;
  registerPage : fromRegisterPage.State;
  dashboardPage : fromDashboardPage.State;
}

export interface State {
  auth: AuthState;
}

export function reducers(state: AuthState | undefined, action: Action) {
  return combineReducers({
    status: fromAuth.reducer,
    loginPage: fromLoginPage.reducer,
    registerPage: fromRegisterPage.reducer,
    dashboardPage: fromDashboardPage.reducer
  })(state, action);
}

export const selectAuthState = createFeatureSelector<State, AuthState>('auth');

export const selectAuthStatusState = createSelector(
  selectAuthState,
  (state: AuthState) => state.status
);
export const getUser = createSelector(selectAuthStatusState, fromAuth.getUser);
export const getLoggedIn = createSelector(getUser, user => !!user);
export const getUserRole = createSelector(selectAuthStatusState, fromAuth.getRole);
export const getAdminMode = createSelector(selectAuthStatusState, fromAuth.getAdminMode);

export const getProfilImage = createSelector(selectAuthStatusState, fromAuth.getProfilImage);
export const getProfilImageError = createSelector(selectAuthStatusState, fromAuth.getError);
export const getCountLikeNews = createSelector(selectAuthStatusState, fromAuth.getCountLikeNews);
export const getPendingAuthStatus = createSelector(selectAuthStatusState, fromAuth.getPendingStatus);

export const selectLoginPageState = createSelector(
  selectAuthState,
  (state: AuthState) => state.loginPage
);
export const selectRegisterPageState = createSelector(
  selectAuthState,
  (state: AuthState) => state.registerPage
)

export const getLoginPageError = createSelector(
  selectLoginPageState,
  fromLoginPage.getError
);
export const getLoginPagePending = createSelector(
  selectLoginPageState,
  fromLoginPage.getPending
);

export const getRegisterPageError = createSelector(
  selectRegisterPageState,
  fromRegisterPage.getError
);
export const getRegisterPagePending = createSelector(
  selectRegisterPageState,
  fromRegisterPage.getError
);

export const selectDashboardPageState = createSelector(
  selectAuthState,
  (state: AuthState) => state.dashboardPage
);
export const getCharacters = createSelector(selectDashboardPageState, fromDashboardPage.getCharacters);
export const getErrorDashboard = createSelector(selectDashboardPageState, fromDashboardPage.getError);