import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Credentials, Account } from '../models';
import * as fromAuth from '../reducers';
import { RegisterPageActions, AuthApiActions } from '../actions';

@Component({
  selector: 'bc-register-page',
  template: `
    <bc-register-form
      (submitted)="onSubmit($event)"
      [errorMessage]="error$ | async"
    >
    </bc-register-form>
  `,
  styles: [],
})
export class RegisterPageComponent implements OnInit {
  error$ = this.store.pipe(select(fromAuth.getRegisterPageError));

  constructor(private store: Store<fromAuth.State>) {}

  ngOnInit() {}

  onSubmit(user: Account) {
    this.store.dispatch(RegisterPageActions.register({ user }));
  }
}