import { Component, OnInit } from '@angular/core';
import * as fromDashboardPage from '../../reducers';
import * as fromAuth from '../../reducers';
import { Store, select } from '@ngrx/store';
import { DashboardPageActions, AuthApiActions } from 'app/auth/actions';

@Component({
  selector: 'dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit {

  constructor(private store: Store<fromDashboardPage.State | fromAuth.State>) { }
  characters$ = this.store.pipe(select(fromDashboardPage.getCharacters));
  user$ = this.store.pipe(select(fromAuth.getUser));
  error$ = this.store.pipe(select(fromAuth.getProfilImageError));
  countlike$ = this.store.pipe(select(fromAuth.getCountLikeNews));
  pending$ = this.store.pipe(select(fromAuth.getPendingAuthStatus));

  ngOnInit() {
    this.store.dispatch(DashboardPageActions.dashboard());
    this.store.dispatch(AuthApiActions.newsCount());
  }

}
