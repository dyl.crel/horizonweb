import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { LoginPageComponent } from '../auth/containers';
import {
  LoginFormComponent,
  LogoutConfirmationDialogComponent,
} from '../auth/components';

import { AuthEffects } from '../auth/effects';
import { reducers } from '../auth/reducers';
import { MaterialModule } from '../material/material.module';
import { AuthRoutingModule } from './auth-routing.module';
import { RegisterFormComponent } from './components/register-form.component';
import { RegisterPageComponent } from './containers/register-page.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DashboardPageComponent } from './containers/dashboard-page/dashboard-page.component';
import { DashboardProfilComponent } from './components/dashboard-profil/dashboard-profil.component';
import { DashboardVoteManagerComponent } from './components/dashboard-vote-manager/dashboard-vote-manager.component';
import { DashboardNavigationComponent } from './components/dashboard-navigation/dashboard-navigation.component';

export const COMPONENTS = [
  LoginPageComponent,
  LoginFormComponent,
  LogoutConfirmationDialogComponent,
  RegisterFormComponent,
  RegisterPageComponent,
  DashboardPageComponent,
  DashboardProfilComponent,
  DashboardNavigationComponent
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    AuthRoutingModule,
    NgbModule,
    FormsModule,
    StoreModule.forFeature('auth', reducers),
    EffectsModule.forFeature([AuthEffects])
  ],
  declarations: COMPONENTS,
  entryComponents: [LogoutConfirmationDialogComponent],
})
export class AuthModule {}
