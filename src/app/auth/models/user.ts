export interface Credentials {
  username: string;
  password: string;
}

export interface Account extends Credentials {
  id: number;
  token: string;
  firstname: string;
  lastname: string;
  nickname: string;
  email: string;
  sponsorid: string;
  secretquestion: string;
  secretanswer: string;
  birthdate: string;
  roleweb: number;
  role: number;
  profilImg: string;
}

export enum RolesEnum {
  User = 1,
  Moderator = 2,
  Administrator = 3
}
export enum GameHierarchyEnum {
  UNAVAILABLE = -1,
  PLAYER = 0,
  MODERATOR = 10,
  GAMEMASTER_PADAWAN = 20,
  GAMEMASTER = 30,
  ADMIN = 40,
  UNKNOW_SPECIAL_USER = 50
}
