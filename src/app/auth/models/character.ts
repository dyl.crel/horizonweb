export interface Character{
    informations: CharacterBaseInformation;
    characterImg: string;
    characteristics: CharacterCharacteristicsInformations;
    initiative: CharacterBaseCharacteristic;
    wisdom: CharacterBaseCharacteristic;
    chance: CharacterBaseCharacteristic;
    agility: CharacterBaseCharacteristic;
    intelligence: CharacterBaseCharacteristic;
    strength: CharacterBaseCharacteristic;
}

export interface CharacterBaseInformation{
    name: string;
    level: number;
}

export interface CharacterCharacteristicsInformations{
    experience: number;
    experienceLevelFloor: number;
    experienceNextLevelFloor: number;
    maxLifePoints: number;
    experiencepourcentage: number;
}
export interface CharacterBaseCharacteristic{
    count: number;
}