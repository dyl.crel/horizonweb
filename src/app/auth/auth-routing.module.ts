import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from '../auth/containers';
import { RegisterPageComponent } from './containers/register-page.component';
import { DisableGuard } from './services/auth-disable-guard.service';
import { DashboardPageComponent } from './containers/dashboard-page/dashboard-page.component';
import { AuthGuard } from './services';
import { LoginGuard } from './services/login-guard.service';

const routes: Routes = [
  { path: 'login', component: LoginPageComponent, data: { title: 'Login' }, canActivate: [DisableGuard] },
  { path: 'register', component: RegisterPageComponent, data: { title: 'Register'}, canActivate: [DisableGuard]},
  { path: 'dashboard', component: DashboardPageComponent, data:{ title: 'Dashboard'}, canActivate: [LoginGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
