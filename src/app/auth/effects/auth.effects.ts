import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';
import {
  LoginPageActions,
  AuthActions,
  AuthApiActions,
  RegisterPageActions,
  DashboardPageActions
} from '../actions';
import { Credentials, Account } from '../models';
import { AuthService } from '../services';
import { LogoutConfirmationDialogComponent } from '../components';
import { dashboardSuccess } from '../actions/dashboard-page.actions';

@Injectable()
export class AuthEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoginPageActions.login),
      map(action => action.credentials),
      exhaustMap((auth: Credentials) =>
        this.authService.login(auth).pipe(
          map(user => AuthApiActions.loginSuccess({ user })),
          catchError(error => of(AuthApiActions.loginFailure({ error })))
        )
      )
    )
  );

  loginSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthApiActions.loginSuccess),
        map(action => action.user),
        tap((user: Account) => {
          if (!localStorage.getItem('nomyaUser')) {
            localStorage.setItem('nomyaUser', JSON.stringify(user.token));
          }
          this.router.navigate(['/']);
        })
      ),
    { dispatch: false }
  );

  loginRedirect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthApiActions.loginRedirect, AuthActions.logout),
        tap(authed => {
          if (!this.authService.getToken()) {
            this.router.navigate(['/login']);
          }
        })
      ),
    { dispatch: false }
  );
  loggedRedirect$ = createEffect(
    () => this.actions$.pipe(
      ofType(AuthApiActions.HasloggedRedirect),
      tap(() => this.router.navigate(['']))
    ),
    { dispatch: false }
  )

  logoutConfirmation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logoutConfirmation),
      exhaustMap(() => {
        const dialogRef = this.dialog.open<
          LogoutConfirmationDialogComponent,
          undefined,
          boolean
        >(LogoutConfirmationDialogComponent);

        return dialogRef.afterClosed();
      }),
      map(
        result =>
          result
            ? AuthActions.logout()
            : AuthActions.logoutConfirmationDismiss()
      )
    )
  );

  register$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RegisterPageActions.register),
      map(action => action.user),
      exhaustMap((user: Account) =>
        this.authService.register(user).pipe(
          map((user) => AuthApiActions.registerSuccess({ user })),
          catchError(error => of(AuthApiActions.registerFailure({ error })))
        )
      )
    )
  );

  registerSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthApiActions.registerSuccess),
      tap(() => this.router.navigate(['/user/login']))
    ),
    { dispatch: false }
  );

  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logout),
      tap(() => this.authService.logout())),
    { dispatch: false }
  );

  autoLogin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthApiActions.autoLogin),
      map(action => action),
      exhaustMap(() =>
        this.authService.autoLogin().pipe(
          map(user => AuthApiActions.autoLoginSuccess({ user })),
          catchError(error => of(AuthApiActions.autoLoginFailure({ error })))
        )
      )
    )
  );
  role$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthApiActions.role),
      map(action => action.credentials),
      exhaustMap((auth: Credentials) =>
        this.authService.getRole(auth).pipe(
          map(role => AuthApiActions.roleSuccess({ role })),
          catchError(error => of(AuthApiActions.roleFailure({ error })))
        )
      )
    )
  );
  autoRole$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthApiActions.autoRole),
      map(action => action),
      exhaustMap(() =>
        this.authService.autoRole().pipe(
          map(role => AuthApiActions.autoRoleSuccess({ role })),
          catchError(error => of(AuthApiActions.autoRoleFailure({ error })))
        )
      )
    )
  );

  adminmode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.adminMode),
      map(action => action.adminMode),
      exhaustMap((adminMode: boolean) =>
        this.authService.getAdminMode(adminMode).pipe(
          map(adminMode => AuthActions.adminModeSuccess({ adminMode })),
        )
      )
    )
  );

  dashboard$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DashboardPageActions.dashboard),
      exhaustMap(() => this.authService.getCharacters().pipe(
        map(characters => DashboardPageActions.dashboardSuccess({ characters })),
        catchError(error => of(DashboardPageActions.dashboardFailure({ error })))
      ))
    )
  );

  uploadImage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthApiActions.uploadProfileImage),
      map(action => action.profilImage),
      exhaustMap((profilImage: string) => this.authService.UploadImageProfil(profilImage).pipe(
        map(user => AuthApiActions.uploadProfileImageSuccess({ user })),
        catchError(error => of(AuthApiActions.uploadProfileImageFailure({ error })))
      ))
    ));

  countLikeNews = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthApiActions.newsCount),
      exhaustMap(() => this.authService.getCountLikeNews().pipe(
        map(count => AuthApiActions.newsCountSuccess({ count }))
      ))
    )
  );

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
    private dialog: MatDialog
  ) { }
}
