export interface News {
    id: number;
    title: string;
    message: string;
    author: string;
    postOn: string;
    img: string;
    like: Like[];
}

export interface Like{
    username: string;
}