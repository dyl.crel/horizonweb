import { Component, OnInit } from '@angular/core';
import { NewsActions } from '../actions';
import * as fromNews from '../reducers/';
import { Store, select } from '@ngrx/store';


@Component({
  selector: 'app-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.scss']
})
export class NewsPageComponent implements OnInit {

  constructor(private store: Store<fromNews.State>) { }

  ngOnInit() {
    this.store.dispatch(NewsActions.news());
  }

}
