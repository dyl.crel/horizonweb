import { Injectable } from "@angular/core";
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { NewsService } from '../services/news.services';
import { MatDialog } from '@angular/material';
import { NewsActions } from '../actions';
import { of } from 'rxjs';
import { exhaustMap, map, catchError } from 'rxjs/operators';
import { News } from '../model/news.models';



@Injectable()
export class NewsEffects {
  news$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NewsActions.news),
      exhaustMap(() =>
        this.news.getNews().pipe(
          map(news => NewsActions.newsSuccess({ news })),
          catchError(error => of(NewsActions.newsFailure({ error })))
        )
      )
    )
  );
  singlenews$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NewsActions.singlenews),
      map(action => action.id),
      exhaustMap((id: string) =>
        this.news.getNewsById(id).pipe(
          map(singlenews => NewsActions.singlenewsSuccess({ singlenews })),
          catchError(error => of(NewsActions.singlenewsFailure({ error })))
        )
      )
    )
  );

  likenews$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NewsActions.likenews),
      map(action => action.id),
      exhaustMap((id: string) =>
        this.news.postLike(id).pipe(
          map(singlenews => NewsActions.likenewsSuccess({ singlenews })),
          catchError(error => of(NewsActions.likenewsFailure({ error })))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private news: NewsService,
    private dialog: MatDialog
  ) { }
}