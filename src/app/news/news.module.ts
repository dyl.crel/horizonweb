import { NgModule } from '@angular/core';
import { NewsViewComponent } from './components/news-view.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material';
import { NewsRoutingModule } from './news-routing.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NewsEffects } from './effects/news.effects';
import { NewsPageComponent } from './containers/news-page.component';
import { reducers } from './reducers';
import { NewsDetailsViewComponent } from './components/news-details-view/news-details-view.component';
import { ScrollingModule } from '@angular/cdk/scrolling';

export const COMPONENTS = [
    NewsViewComponent,
    NewsPageComponent,
    NewsDetailsViewComponent
]

@NgModule({
    imports: [
      CommonModule,
      ReactiveFormsModule,
      MaterialModule,
      NewsRoutingModule,
      StoreModule.forFeature('news', reducers),
      EffectsModule.forFeature([NewsEffects]),
      ScrollingModule
    ],
    declarations: COMPONENTS,
  })
  export class NewsModule {}