import {
    createSelector,
    createFeatureSelector,
    Action,
    combineReducers,
  } from '@ngrx/store';
  import * as fromNews from '../reducers/news.reducers';
  
  export interface NewsState {
    newsPage: fromNews.State
  }
  
  export interface State {
    news: NewsState;
  }
  
  export function reducers(state: NewsState | undefined, action: Action) {
    return combineReducers({
      newsPage: fromNews.reducer
    })(state, action);
  }
  
  export const selectNewsState = createFeatureSelector<State, NewsState>('news');
  
  export const selectNewsPageState = createSelector(
    selectNewsState,
    (state: NewsState) => state.newsPage
  );
  export const getNewss = createSelector(selectNewsPageState, fromNews.getNews);

  export const getSingleNews = createSelector(selectNewsPageState, fromNews.getSingleNews);
  
  