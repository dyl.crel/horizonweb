import { News } from '../model/news.models';
import { createReducer, on } from '@ngrx/store';
import { NewsActions } from '../actions';

export interface State {
    news : News[] | null,
    singlenews : News[] | null
}

export const initialState: State = {
    news: null,
    singlenews: null
}

export const reducer = createReducer(
    initialState,

    on(NewsActions.news, state => ({
        ...state
    })),

    on(NewsActions.newsSuccess, (state, {news}) => ({
        ...state,
        news
    })),

    on(NewsActions.newsFailure, state => ({
        ...state
    })),

    on(NewsActions.singlenews, state => ({
        ...state
    })),

    on(NewsActions.singlenewsSuccess, (state, {singlenews}) => ({
        ...state,
        singlenews
    })),

    on(NewsActions.singlenewsFailure, state => ({
        ...state
    })),

    on(NewsActions.likenews, state => ({
        ...state
    })),

    on(NewsActions.likenewsSuccess, (state, {singlenews}) => ({
        ...state,
        singlenews
    })),

    on(NewsActions.likenewsFailure, state => ({
        ...state
    }))
)

export const getNews = (state: State) => state.news;
export const getSingleNews = (state: State) => state.singlenews;