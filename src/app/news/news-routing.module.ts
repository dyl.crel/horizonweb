import { NgModule } from "@angular/core";
import { NewsPageComponent } from './containers/news-page.component';
import { Routes, RouterModule } from '@angular/router';
import { NewsDetailsViewComponent } from './components/news-details-view/news-details-view.component';

const routes: Routes = [
    { path: '', component: NewsPageComponent, data: { title: 'Home' } },
    {
      path: 'news/:id/details',
      component: NewsDetailsViewComponent,
      data: {title: 'News Details'}
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  
  export class NewsRoutingModule{}