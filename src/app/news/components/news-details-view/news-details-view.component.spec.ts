import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsDetailsViewComponent } from './news-details-view.component';

describe('NewsDetailsViewComponent', () => {
  let component: NewsDetailsViewComponent;
  let fixture: ComponentFixture<NewsDetailsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsDetailsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsDetailsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
