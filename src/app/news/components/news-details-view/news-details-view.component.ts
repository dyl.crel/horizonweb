import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import * as fromNews from '../../reducers/';
import { NewsActions } from 'app/news/actions';
import * as fromAuth from '../../../auth/reducers'

@Component({
  selector: 'app-news-details-view',
  templateUrl: './news-details-view.component.html',
  styleUrls: ['./news-details-view.component.scss']
})
export class NewsDetailsViewComponent implements OnInit {

  singlenews$ = this.store.pipe(select(fromNews.getSingleNews));
  user$ = this.store.pipe(select(fromAuth.getUser));
  constructor(private activatedRoute: ActivatedRoute, private store : Store<fromNews.State | fromAuth.State>) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.store.dispatch(NewsActions.singlenews({id}));
  }

  public Like():void{
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.store.dispatch(NewsActions.likenews({id}));
  }

}
