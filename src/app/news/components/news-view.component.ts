import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';

import * as fromNews from '../reducers/';

@Component({
  selector: 'bc-news',
  templateUrl: './news-view.component.html',
  styleUrls: ['./news-view.component.scss']
})
export class NewsViewComponent implements OnInit {
  
  news$ = this.store.pipe(select(fromNews.getNewss));
  constructor(private store: Store<fromNews.State>) { 
  }
  
  ngOnInit() {

  }

}
