import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { News } from '../model/news.models';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class NewsService {
  constructor(private http: HttpClient) { }

  getNews(): Observable<News[]> {
    return this.http.get<News[]>('https://nomya.net:44373/api/news');
  }

  getNewsById(id: string) : Observable<News[]>{
    return this.http.get<News[]>('https://nomya.net:44373/api/news/single/'+ id)
  }

  postLike(id: string) : Observable<News[]>{
    return this.http.post<News[]>('https://nomya.net:44373/api/news/like/'+ id, {})
  }
}
