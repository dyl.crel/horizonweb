import { createAction, props } from '@ngrx/store';
import { News } from '../model/news.models';



export const news = createAction('[News] News');
export const newsSuccess = createAction('[News] News Success', props<{news: News[]}>());
export const newsFailure = createAction('[News] News Failure', props<{ error : any}>());

export const singlenews = createAction('[News] New', props<{id: string}>());
export const singlenewsSuccess = createAction('[News] New Success', props<{singlenews: News[]}>());
export const singlenewsFailure = createAction('[News] New Failure', props<{ error : any}>());

export const likenews = createAction('[News] Like', props<{id: string}>());
export const likenewsSuccess = createAction('[News] Like Success', props<{singlenews: News[]}>());
export const likenewsFailure = createAction('[News] Like Failure', props<{ error : any}>());

