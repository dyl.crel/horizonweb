import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './auth/services';
import { NotFoundPageComponent,AppComponent } from './core/containers';
import { NewsPageComponent } from './news/containers/news-page.component';
import { NewsDetailsViewComponent } from './news/components/news-details-view/news-details-view.component';

export const routes: Routes = [
  {
    path: 'user',
    loadChildren: './auth/auth.module#AuthModule'
  },
  {
    path: '',
    loadChildren: './news/news.module#NewsModule'
  },
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule'
  },
  {
    path:'**',
    component: NotFoundPageComponent,
    data: {title: '404 not found'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}