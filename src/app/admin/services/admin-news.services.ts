import { News } from 'app/news/model/news.models';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
  })
  export class AdminService {
    constructor(private http: HttpClient) { }

    postNews(news: News) : Observable<any>{
        return this.http.post<News>('https://nomya.net:44373/api/news/add', news);
    }
  }  