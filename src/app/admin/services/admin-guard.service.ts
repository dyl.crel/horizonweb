import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import * as fromAuth from '../../auth/reducers';
import { RolesEnum } from 'app/auth/models';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate {
  constructor(private store: Store<fromAuth.State>) { }

  canActivate(): Observable<boolean> {
      return this.store.pipe(select(fromAuth.getUserRole),
      map(rolesenum => {
          if(rolesenum === RolesEnum.Administrator){
              return true;
          }
          return false;
      }));
  }
}
