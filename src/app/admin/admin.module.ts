import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AdminNewsPageComponent } from './containers/admin-news-page.component';
import { AdminRoutingModule } from './admin-routing.module';
import { QuillModule } from 'ngx-quill';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminNewsFormComponent } from './components/admin-news-form.component';
import { AdminNewsViewerComponent } from './components/admin-news-viewer.component';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../admin/reducers';
import { EffectsModule } from '@ngrx/effects';
import { AdminEffects } from './effects/admin-news.effect';

@NgModule({
  declarations: [AdminNewsPageComponent, AdminNewsFormComponent, AdminNewsViewerComponent],
  imports: [
    AdminRoutingModule,
    CommonModule,
    QuillModule.forRoot(),
    ReactiveFormsModule,
    StoreModule.forFeature('admin', reducers),
    EffectsModule.forFeature([AdminEffects])
  ],
})
export class AdminModule { }
