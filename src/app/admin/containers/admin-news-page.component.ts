import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-admin-news-page',
  templateUrl: './admin-news-page.component.html',
  styleUrls: ['./admin-news-page.component.scss']
})
export class AdminNewsPageComponent implements OnInit {

  title: string | null;
  img: string | null;
  body: string | null;
  constructor() { }

  ngOnInit() {
  }

  onTitleChange(value){
    this.title = value;
  }
  onImgChange(value){
    this.img = value;
  }
  onBodyChange(value){
    this.body = value;
  }
}
