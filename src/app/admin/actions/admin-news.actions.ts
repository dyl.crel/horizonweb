import { createAction, props } from '@ngrx/store';
import { News } from '../../news/model/news.models';


export const PostNews = createAction('[Admin] Post News', props<{news : News}>());
export const PostNewsSuccess = createAction('[Admin] Post News Success', props<{message : any}>());
export const PostNewsFailure = createAction('[Admin] Post News Failure', props<{error : any}>());