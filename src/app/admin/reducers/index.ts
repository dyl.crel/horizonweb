import * as fromAdminNewsPage from '../reducers/admin-news.reducer';
import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';


export interface AdminState{
    AdminNewsPage: fromAdminNewsPage.State;
}

export interface State{
    admin: AdminState;
}

export function reducers(state: AdminState | undefined, action: Action){
    return combineReducers({
        AdminNewsPage: fromAdminNewsPage.reducer
    })(state, action);
}

export const selectAdminState = createFeatureSelector<State,AdminState>('admin');