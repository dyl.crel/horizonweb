import { createReducer, on } from '@ngrx/store';
import { AdminNewsActions } from '../actions';
import { News } from 'app/news/model/news.models';


export interface State {
    message: string | null;
    error: string | null;
    news: News | null;
}

export const initialState: State = {
    message: null,
    error: null,
    news: null
}

export const reducer = createReducer(
    initialState,
    on(AdminNewsActions.PostNews, (state, {news}) => ({...state, news})),
    on(AdminNewsActions.PostNewsSuccess, (state, {message}) => ({...state, message})),
    on(AdminNewsActions.PostNewsFailure,(state, {error}) => ({...state, error}))
);