import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { QuillEditorComponent } from 'ngx-quill';
import { Store } from '@ngrx/store';
import * as fromAdmin from '../reducers';
import { AdminNewsActions } from '../actions';
import { News } from 'app/news/model/news.models';

@Component({
  selector: 'nomya-admin-news-form',
  templateUrl: './admin-news-form.component.html',
  styleUrls: ['./admin-news-form.component.scss']
})
export class AdminNewsFormComponent implements OnInit {

  //declarations
  title = 'Quill works!';
  hide = false;
  isReadOnly = false;
  form: FormGroup;

  @Output() titleNews = new EventEmitter<string>();
  @Output() imgUrl = new EventEmitter<string>();
  @Output() bodyNews = new EventEmitter<string>();
  
  constructor(fb: FormBuilder, private store: Store<fromAdmin.State>) {
    this.form = fb.group({
      message: [''],
      title: [''],
      img: ['']
    });
  }
  @ViewChild('editor', {static: true}) editor: QuillEditorComponent

  ngOnInit() {
  }

  titleChange(value){
    this.titleNews.emit(value);
  }
  urlChange(value){
    this.imgUrl.emit(value);
  }
  bodyChange(value){
    this.editor
      .onContentChanged
      .pipe(
        distinctUntilChanged()
      )
      .subscribe(data => {
        this.bodyNews.emit(data.html);
      });
  }

  submit(){
    let news : News = this.form.value;
    this.store.dispatch(AdminNewsActions.PostNews({news}));
  }

}

