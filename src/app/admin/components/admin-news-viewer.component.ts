import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'nomya-admin-news-viewer',
  templateUrl: './admin-news-viewer.component.html',
  styleUrls: ['./admin-news-viewer.component.scss']
})
export class AdminNewsViewerComponent implements OnInit {

  @Input() titleNews: string | null;
  @Input() test: string | null;
  @Input() bodyNews: string | null;
  constructor() { }

  ngOnInit() {
  }

}
