import { Injectable } from "@angular/core";
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AdminNewsActions } from '../actions';
import { exhaustMap, map, catchError } from 'rxjs/operators';
import { News } from '../../news/model/news.models';
import { AdminService } from '../services/admin-news.services';
import { of } from 'rxjs';


@Injectable()
export class AdminEffects {
    login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AdminNewsActions.PostNews),
      map(action => action.news),
      exhaustMap((news: News) =>
        this.adminService.postNews(news).pipe(
          map(message => AdminNewsActions.PostNewsSuccess({ message })),
          catchError(error => of(AdminNewsActions.PostNewsFailure({ error })))
        )
      )
    )
  );


    constructor(
        private actions$: Actions,
        private router: Router,
        private dialog: MatDialog,
        private adminService: AdminService
    ) { }
}