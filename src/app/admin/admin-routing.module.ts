import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminNewsPageComponent } from './containers/admin-news-page.component';
import { Routes, RouterModule } from '@angular/router';
import { AdminNewsFormComponent } from './components/admin-news-form.component';
import { AdminNewsViewerComponent } from './components/admin-news-viewer.component';
import { AdminGuard } from './services/admin-guard.service';

const routes : Routes = [
  { path : 'news', component: AdminNewsPageComponent, data: {title: "Admin News"}, canActivate: [AdminGuard]}
]


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
