import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import { AuthService } from 'app/auth/services';
@Injectable({
  providedIn: 'root'
})
export class JwtinterceptorService implements HttpInterceptor {
  constructor(private authService: AuthService){}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    let currentUser = JSON.parse(localStorage.getItem('nomyaUser'));
    if (currentUser) {
        request = request.clone({
            setHeaders: { 
                Authorization: `Bearer ${currentUser}`
            }
        });
    }

    return next.handle(request);
  }
}
