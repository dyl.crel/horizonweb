import { Component, ViewContainerRef, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { AuthService } from './auth/services';
import { Store, select } from '@ngrx/store';
import * as fromAuth from '../app/auth/reducers';
import * as fromRoot from '../app/reducers';
import { AuthApiActions } from './auth/actions';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

    subscription: Subscription;

    constructor(authService: AuthService, private router: Router, private store: Store<fromRoot.State & fromAuth.State>) {

    }
    loggedIn$ = this.store.pipe(select(fromAuth.getLoggedIn));
    loggedRole$ = this.store.pipe(select(fromAuth.getUserRole));
    adminMode$ = this.store.pipe(select(fromAuth.getAdminMode));

    ngOnInit() {
        this.subscription = this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd)
            )
            .subscribe(() => window.scrollTo(0, 0));
        if (localStorage.getItem('nomyaUser')) {
            this.store.dispatch(AuthApiActions.autoLogin());
            this.store.dispatch(AuthApiActions.autoRole());
        }
    }


    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }



}