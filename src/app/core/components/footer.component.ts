import { Component, HostBinding } from '@angular/core';

@Component({
    selector: 'bc-footer',
    template: `
    <footer>
        <div class="container-fluid">
            <p class="copyright text-center">
                Copyright  &copy;  {{currentDate | date: 'yyyy'}} <a href="#">Horizon</a>, All rights reserved.          
             </p>
        </div>
    </footer>
    `,
    styles: []
})

export class FooterComponent {
    currentDate: Date = new Date();
}