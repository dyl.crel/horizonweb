import { Component } from '@angular/core';

@Component({
  selector: 'bc-layout',
  template: `
    <mat-sidenav-container fullscreen class="background">

      <ng-content></ng-content>

    </mat-sidenav-container>
  `,
  styles: [
    `
      // .background{
      //   background-image: url(../../../assets/img/background.png);
      //   background-repeat: no-repeat;
      //   background-position: center;
      //   background-size: cover;
      // }
      mat-sidenav-container {
        background: rgba(0, 0, 0, 0.03);
      }

      *,
      /deep/ * {
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
      }
    `,
  ],
})
export class LayoutComponent {}
