import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'bc-toolbar',
  template: `
    <mat-toolbar class="toolbar">
      <ng-content></ng-content>
    </mat-toolbar>
  `,
  styles: [`
  .toolbar{
    background-color: #FBFBFB;
  }
  `]
})
export class ToolbarComponent {
  @Output() openMenu = new EventEmitter();
}