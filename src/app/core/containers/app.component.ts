import { ChangeDetectionStrategy, Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AuthActions } from '../../auth/actions';
import * as fromAuth from '../../auth/reducers';
import * as fromRoot from '../../reducers';
import { LayoutActions } from '../../core/actions';

@Component({
  selector: 'bc-app',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <bc-layout>
      <bc-toolbar class="header">
        <h1>Nomya</h1>
        <span class="example-spacer"></span>
        <button mat-button routerLink=""><i class="fas fa-home"></i> Home</button>
        <button *ngIf="!(loggedIn$ | async)" mat-button routerLink="/login"><i class="fas fa-sign-in-alt"></i> Login</button>
        <button *ngIf="!(loggedIn$ | async)" mat-button routerLink="/register"><i class="fas fa-user-plus"></i> Register</button>
        <button mat-button routerLink="/forum"><i class="fas fa-external-link-alt"></i> Forum</button>
        <button *ngIf="(loggedIn$ | async)" mat-button (click)="logout()"><i class="fas fa-sign-out-alt"></i> Logout</button>
      </bc-toolbar>

      <router-outlet ></router-outlet>
      <bc-footer></bc-footer>
    </bc-layout>
  `,
  styles: [
  `
  .header{
    position: fixed;
    top: 0;
    width: 100%;
  }
  
  .example-spacer {
    flex: 1 1 auto;
  }
  button{
    margin-right: 5px;
  }
  `
  ]
})
export class AppComponent {
  showSidenav$: Observable<boolean>;
  loggedIn$: Observable<boolean>;

  constructor(private store: Store<fromRoot.State & fromAuth.State>) {
    /**
     * Selectors can be applied with the `select` operator which passes the state
     * tree to the provided selector
     */
    this.loggedIn$ = this.store.pipe(select(fromAuth.getLoggedIn));
  }

  logout() {
    this.store.dispatch(AuthActions.logoutConfirmation());
  }
}
